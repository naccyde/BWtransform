#ifndef _REVERSE_BURROWS_WHEELER_H
#define _REVERSE_BURROWS_WHEELER_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int *frequencies(const char *t, int n);
char *sortchar(const char *t, int n);
int *find_indexes(const char *t, int n);
char *bwreverse(const char *t, int n);

#endif
