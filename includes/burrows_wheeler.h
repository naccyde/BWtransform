#ifndef _BURROWS_WHEELER_H
#define _BURROWS_WHEELER_H

#include <stdio.h>
#include <stdlib.h>

int compare_rotations(const char *t, int n, int fst, int snd);
void sort(int *r, int n, const char *t, int t_size);
int swap(int *t, int pivot, int index_to_move);
char *bwtransform(const char *t, int n);

#endif
