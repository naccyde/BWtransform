#include "burrows_wheeler.h"

/* compare_rotations : compare text rotations */
int compare_rotations(const char *t, int n, int fst, int snd)
{
        int i;
        for (i = 0; i < n && t[(fst+i) % n] == t[(snd+i) % n]; ++i) ;

        return t[(fst+i) % n] - t[(snd+i) % n];
}

/* sort : quick sort for Burrows-Wheeler rotations */
void sort(int *r, int n, const char *t, int t_size)
{
        if (n <= 1)
                return;

        int pivot = 0;
       
        for (int i = 1; i < n; i++) {
                if (compare_rotations(t, t_size, r[i], r[pivot]) < 0) 
                        pivot = swap(r, pivot, i);
        }                

        sort(&r[0], pivot+1, t, t_size);
        sort(&r[pivot+1], (n-pivot)-1, t, t_size);
}

/* swap : swap inline to int in t for quick sort */
int swap(int *t, int pivot, int index_to_move)
{
        int tmp = t[index_to_move];
        
        for(int i = index_to_move; i != pivot; --i)
                t[i] = t[i-1];

        t[pivot] = tmp;

        return pivot + 1;
}

/* bwtransform : operate the Burrows-Wheeler transform of the string t */
char *bwtransform(const char *t, int n)
{
	char *final = malloc((n + 1) * sizeof(char)); /* n+1 for trailing key */
	
	if (! final) {
		printf("ERROR : Could not allocate memory for final array.\n");
		goto err;
	}

	int *r = malloc(n * sizeof(int));
	
	if (! r) {
		free(final);
		printf("ERROR : Could not allocate memory for rotations.\n");
		goto err;
	}

	for(int i = 0; i < n; ++i) {
		r[i] = i;
	}

	sort(r, n, t, n);

	for(int i = 0; i < n; ++i) {
		int index = (r[i] + (n - 1)) % n;
		final[i]= t[index];

		if (r[i] == 1)
			final[n] = i;
	}

	free(r);
	
	return final;

err:
	return NULL;
}
