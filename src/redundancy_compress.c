#include "redundancy_compress.h"

/* compute_marker: compute the marker of the t array */
int compute_marker(const char *t, int n)
{
        int *r = occurences(t, n);

        if (! r) {
                printf("ERROR : No occurence array\n");
                return NULL;
        }

        int min = 0;
  
        for (int i = 1; i < 128; ++i) {
                if (r[i] < r[min])
                        min = i;                
        }
    
        free(r);
        return min;
}

/* occurences: compute occurences of each letter in t */
int *occurences(const char *t, int n)
{
        int *r = malloc(128 * sizeof(int));

        if (! r) {
                printf("ERROR : Could not alloc memory\n");
                return NULL;
        }

        for (int i = n-1; i--; ) {
                ++r[(int)t[i]];
        }

        return r;
}

/* compressed_size: return the compressed size of the text */
int compressed_size(const char *t, int n)
{
        int size = 1; /* 1 for the marker */
        
        for (int i = 0; i < n-1; i++) {
                int tmp = i;
                if (i+1 < n && t[i] == t[i+1])
                        ++i;
        
                if (tmp == i)
                        size += 1;
                else
                        size += 3;
        }

        return size;
}

/* compress: compress t string by redundancy */
char *compress(const char *t, int n, int *rsize) 
{
        int marker = compute_marker(t, n);

        *rsize = compressed_size(t, n);
        char *r = malloc((*rsize) * sizeof(char));
        
        if (! r) {
                printf("ERROR : Coud not alloc memory for commpressed string\n");
                return NULL;
        }

        r[0] = marker;
        int ri = 1;
        for (int i = 0; i < n; ++i) {
                int tmp = i;

                while (i+1 < n && t[i] == t[i+1])
                        ++i;

                if (tmp == i) {
                        r[ri] = t[i];
                        ++ri;
                } else {
                        r[ri] = marker;
                        r[ri+1] = (i - tmp) + 1;
                        r[ri+2] = t[i];
                        ri += 3;
                }
        }
        
        return r;
}
