#include <stdio.h>
#include "redundancy_compress.h"
#include "burrows_wheeler.h"
#include "reverse_burrows_wheeler.h"

int main(int argc, char *argv[])
{
        char *str;
        if (argc == 2)
                str = argv[1];
        else
                str = "How can you own [...] numbers ? Numbers belong to the world.";
        
        printf("Original :\n\t%s\n", str);
	int n = strlen(str);

        /* BW transform */
	char *t = malloc(n * sizeof(char));
	
	if (! t) {
		printf("ERROR : Could not allocate space.\n");
		goto err;
	}

	strncpy(t, str, n); /* Remove trailing \0 */

	char *transformed = bwtransform(t, n);
	
	if (! transformed) {
		free(t);
		printf("ERROR : No transformed text returned.\n");
		goto err;
	}

        printf("Tranformed :\n\t");
        for(int i = 0; i < n+1; ++i)
                printf("%c", transformed[i]);
        printf("\n");
 
        /* Redundancy compress */
        int size;
        char *r = compress(transformed, n+1, &size);
        
	if (! r) {
		free(t);
		free(transformed);
		printf("ERROR : No text compressed returned.\n");
		goto err;
	}

        printf("Compressed :\n\t");     
        for(int i = 0; i < size; ++i)
                printf(i == size-1 ? "[%d, %c]" : "[%d, %c], ", r[i], r[i]);
        printf("\n");

        /* BW Reverse */
        char *t_reverse = bwreverse(transformed, n+1);
        
	if (! t_reverse) {
		free(r);
		free(transformed);
		free(t);
		printf("ERROR : No transformed text returned.\n");
		goto err;
	}

	printf("Reversed :\n\t");
        for(int i = 0; i < n; ++i)
                printf("%c", t_reverse[i]);
        printf("\n");


	free(t);
        free(transformed);
        free(r);
        free(t_reverse);
	return EXIT_SUCCESS;

err:
	return EXIT_FAILURE;	
}
