# BURROWS-WHEELER TRANSFORM

Burrows-Wheeler transform is used in data compressing. The transformation rearranges a character string into runs of similar characters. This exercise comes from a French Polytechnic School contest subject.
More informations on Wikipedia : [B-W Transform](http://www.wikiwand.com/en/Burrows%E2%80%93Wheeler_transform)


## How to use it

```
mkdir bin
```

Create the folder where the project will be build

```
make
```

Build the project

```
./bin/out
```

Transform and compress the default sentence
```
./bin/out my_sentence_here
```

Transform and compress your sentence


## To Do

* Fixe issues
* Implement MTF (Move To Front) algorithm


## To know

* By default, the project is build with -g flag (retain symbols informations for debugging). You can delete it by modifying the CFLAG var in Makefile
* I followed the [Kernel Coding Guidelines](https://www.kernel.org/doc/Documentation/CodingStyle)
